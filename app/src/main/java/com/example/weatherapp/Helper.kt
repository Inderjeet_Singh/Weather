package com.example.weatherapp

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast
import com.example.weatherapp.interfaces.BackendApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Helper {
    fun checkNetwork(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            return if (networkInfo != null) {
                val isAvailability = networkInfo.isConnected
                if (!isAvailability) {
                    toastMessage(context, "No network connection")
                }
                isAvailability
            } else {
                toastMessage(context, "No network connection")
                false
            }
        } else {
            toastMessage(context, "No network connection")
            false
        }
    }

    private fun toastMessage(context: Context, message: String) {
        makeToast(context, message)
    }

    fun makeToast(localContext: Context, message: String) {
        Toast.makeText(localContext, message, Toast.LENGTH_LONG).show()
    }

    private fun getRetrofitClient(url: String): Retrofit {
        return Retrofit.Builder().baseUrl(url).client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    fun getBackendAPI(baseUrl: String): BackendApi {
        return getRetrofitClient(baseUrl).create(BackendApi::class.java)
    }

    fun convertMPcToKMPH(msec: Double): Double {
        return Math.rint(3.6 * msec)
    }

    fun convertKTOC(degrees: Double): String {
        return "${Math.rint(degrees - 273.15)} °C"
    }

    @SuppressLint("SimpleDateFormat")
    fun convertMilliToStringTime(timeInMillis: Long, timezone: String): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timeInMillis
        calendar.timeZone = TimeZone.getTimeZone(timezone)
        val sdf = SimpleDateFormat("h:mm a")
        sdf.timeZone = TimeZone.getTimeZone(timezone)
//        sdf.timeZone = TimeZone.getDefault()
        return sdf.format(calendar.time)


//        return "${calendar.get(Calendar.HOUR)}:${calendar.get(Calendar.MINUTE)}"
    }

    @SuppressLint("SimpleDateFormat")
    fun convert24to12(time: String): String{
        return try {
            val sdf = SimpleDateFormat("H:mm")
            val dateObj = sdf.parse(time)
            SimpleDateFormat("h:mm a").format(dateObj)
        } catch (e: ParseException) {
            e.printStackTrace()
            time
        }
    }

    fun convertMilliToStringDate(timeInMillis: Long): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date(timeInMillis)
        @SuppressLint("SimpleDateFormat") val sdf = SimpleDateFormat("yyyy-MM-dd")
        sdf.timeZone = TimeZone.getDefault()
        Log.d("yyyy", sdf.format(calendar.time))
        return sdf.format(calendar.time)
    }

    fun getTomorrowMillis(): Long{
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.add(Calendar.DAY_OF_MONTH, 1)

        return calendar.timeInMillis
    }

    @SuppressLint("SimpleDateFormat")
    fun getDayName(date: String): String{
        return SimpleDateFormat("EEEE").format(SimpleDateFormat("yyyy-MM-dd").parse(date))
    }

    @SuppressLint("SimpleDateFormat")
    fun changeDatePattern(date: String): String{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat = SimpleDateFormat("dd MMMM yyyy")

        return try {
            outputFormat.format(inputFormat.parse(date))
        } catch (e: ParseException) {
            e.printStackTrace()
            date
        }
    }
}