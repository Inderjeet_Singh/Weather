package com.example.weatherapp.interfaces


import retrofit2.http.GET
import retrofit2.http.Query
import okhttp3.ResponseBody
import retrofit2.Call

interface BackendApi {
    @GET("weather")
    fun getCurrentWeather(@Query("appid") appId: String, @Query("q") q: String): Call<ResponseBody>

    @GET("forecast")
    fun getForecastWeather(@Query("appid") appId: String, @Query("q") q: String): Call<ResponseBody>

    @GET("json")
    fun getTimeZone(@Query("key") key: String, @Query("timestamp") timestamp: String, @Query(
        "location") location: String): Call<ResponseBody>
}