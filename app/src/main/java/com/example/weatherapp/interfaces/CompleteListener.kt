package com.example.weatherapp.interfaces

interface CompleteListener {
    fun onComplete()
    fun onCancel()
}