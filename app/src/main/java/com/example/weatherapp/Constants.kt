package com.example.weatherapp

object Constants {
    const val APP_ID = "093948889180a0d6d054400171df39c2"
    const val GOOGLE_API_KEY = "AIzaSyAIUahFUPLlDnu5G2NHC0-ALJGfRsV7Pds"

    const val IMAGE_URL = "http://openweathermap.org/img/w/"
    const val CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/"
    const val TIMEZONE_URL = "https://maps.googleapis.com/maps/api/timezone/"

}