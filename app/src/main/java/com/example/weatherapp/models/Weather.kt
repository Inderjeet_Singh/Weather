package com.example.weatherapp.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Weather(
    var temp: Double = 0.0,
    var wind: Double = 0.0,
    var humidity: Int = 0,
    var pressure: Double = 0.0,
    var description: String = "",
    var icon: String = "",
    var time: Long = 0,
    var dateTime: String = "",

    var other: Other = Other()
): Parcelable