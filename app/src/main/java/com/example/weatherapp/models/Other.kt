package com.example.weatherapp.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Other(
    var sunrise: Long = 0,
    var sunset: Long = 0,
    var city: String = "",
    var country: String = "",
    var lat: Double = 0.0,
    var lon: Double = 0.0,
    var timeZone: String = ""
): Parcelable