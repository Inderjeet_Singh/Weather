package com.example.weatherapp.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.example.weatherapp.Constants
import com.example.weatherapp.Helper
import com.example.weatherapp.models.Weather
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_weather.view.*

class WeatherViewHolder(private val context: Context, val view: View): WeatherAdapter.RootViewHolder(view), LayoutContainer {
    @SuppressLint("SetTextI18n")
   override fun bindTo(weather: Any){
        (weather as Weather)
            .let {
                Glide.with(context).load("${Constants.IMAGE_URL}${it.icon}.png").into(view.iv_weather)
                view.tv_time.text = Helper.convert24to12(it.dateTime.split(" ")[1])
                view.tv_temp.text = Helper.convertKTOC(it.temp)

                view.tv_wind.text = "Wind: ${Helper.convertMPcToKMPH(it.wind)} km/h"
                view.tv_pressure.text = "Pressure: ${it.pressure} hpa"
                view.tv_humidity.text = "Humidity: ${it.humidity} %"
            }
    }
}