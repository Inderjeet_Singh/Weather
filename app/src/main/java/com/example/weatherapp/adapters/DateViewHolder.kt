package com.example.weatherapp.adapters

import android.annotation.SuppressLint
import android.view.View
import com.example.weatherapp.Helper
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_date.view.*

class DateViewHolder(val view: View): WeatherAdapter.RootViewHolder(view), LayoutContainer {
    @SuppressLint("SetTextI18n")
    override fun bindTo(item: Any) {
        (item as String).let {
            view.tv_date.text = "${Helper.getDayName(it)}, ${Helper.changeDatePattern(it)}"
        }
    }
}