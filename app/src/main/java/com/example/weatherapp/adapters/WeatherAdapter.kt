package com.example.weatherapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.WeatherDiffItem
import com.example.weatherapp.models.Weather
import kotlinx.android.extensions.LayoutContainer

class WeatherAdapter(private val context: Context): ListAdapter<Any, WeatherAdapter.RootViewHolder>(WeatherDiffItem){
    companion object {
        const val VIEW_TYPE_WEATHER = 0
        const val VIEW_TYPE_DATE = 1
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item is Weather) {
            VIEW_TYPE_WEATHER
        } else {
            VIEW_TYPE_DATE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RootViewHolder {
        return when (viewType) {
            VIEW_TYPE_WEATHER -> {
                WeatherViewHolder(context, LayoutInflater.from(parent.context).inflate(R.layout.item_weather, parent, false))
            }
            else -> {
                DateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_date, parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: RootViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    abstract class RootViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        abstract fun bindTo(item: Any)
    }

}