package com.example.weatherapp.view

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.R
import com.example.weatherapp.adapters.WeatherAdapter
import kotlinx.android.synthetic.main.tomorrow_fragment.*


class TomorrowFragment : Fragment() {

    companion object {
        fun newInstance() = TomorrowFragment()
    }

    private lateinit var viewModel: TomorrowViewModel
    private lateinit var activityViewModel: MainViewModel
    private lateinit var adapter: WeatherAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.tomorrow_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TomorrowViewModel::class.java)
        activityViewModel = ViewModelProviders.of(requireActivity())[MainViewModel::class.java]

        setAdapter()
    }

    private fun setAdapter(){
        adapter = viewModel.getWeatherAdapter(requireContext())
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        recycler_view.adapter = adapter

        notifyAdapter()
    }

    fun notifyAdapter(){
        if(isVisible) {
            adapter.submitList(activityViewModel.tomorrowList)
            adapter.notifyDataSetChanged()
        }
    }
}
