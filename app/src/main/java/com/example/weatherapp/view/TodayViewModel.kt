package com.example.weatherapp.view

import android.content.Context
import androidx.lifecycle.ViewModel;
import com.example.weatherapp.adapters.WeatherAdapter

class TodayViewModel : ViewModel() {
  private var weatherAdapter: WeatherAdapter? = null

    fun getWeatherAdapter(context: Context): WeatherAdapter{
        if(weatherAdapter == null){
            weatherAdapter = WeatherAdapter(context)
        }
        return weatherAdapter!!
    }

}
