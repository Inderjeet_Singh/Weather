package com.example.weatherapp.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.weatherapp.Constants.IMAGE_URL
import com.example.weatherapp.Helper.checkNetwork
import com.example.weatherapp.Helper.convertKTOC
import com.example.weatherapp.Helper.convertMPcToKMPH
import com.example.weatherapp.Helper.convertMilliToStringTime
import com.example.weatherapp.R
import com.example.weatherapp.adapters.PagerAdapter
import com.example.weatherapp.interfaces.CompleteListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_current_weather.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        setToolBar()
        checkNetwork(this)

        setPagerAdapter()
        getWeather("Chandigarh")
    }

    private fun setToolBar() {
        setSupportActionBar(tool_bar)
        supportActionBar!!.title = ""
        search_view.setOnQueryTextListener(object: androidx.appcompat.widget.SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                getWeather(query!!)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                getWeather(newText!!)
                return true
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.refresh_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                getWeather("Chandigarh")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setPagerAdapter() {
        val fragmentList = ArrayList<Fragment>()
        fragmentList.add(viewModel.todayFragment)
        fragmentList.add(viewModel.tomorrowFragment)
        fragmentList.add(viewModel.daysFragment)

        val mPagerAdapter = PagerAdapter(supportFragmentManager, fragmentList)
        val mViewPager = view_pager_container
        mViewPager.adapter = mPagerAdapter
        val tabLayout = tab_layout
        tabLayout.setupWithViewPager(mViewPager)
    }

    private fun getWeather(city: String) {
        viewModel.getWeather(city, object : CompleteListener {
            override fun onComplete() {
                setDataOnField()
                getForecast(city)
            }

            override fun onCancel() {
                getWeather(city)
            }
        })
    }

    private fun getForecast(city: String) {
        viewModel.getForecast(city, object : CompleteListener {
            override fun onComplete() {
                viewModel.notifyFragmentAdapter()
            }

            override fun onCancel() {

            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setDataOnField() {
        Glide.with(this).load("$IMAGE_URL${viewModel.weather.icon}.png").into(iv_weather)

        viewModel.getTimeZone(object : CompleteListener {
            override fun onCancel() {

            }

            override fun onComplete() {
                tv_time.text = convertMilliToStringTime(System.currentTimeMillis(), viewModel.weather.other.timeZone)
                tv_sunrise.text = "Sunrise: ${convertMilliToStringTime(viewModel.weather.other.sunrise,
                    viewModel.weather.other.timeZone)}"
                tv_sunset.text = "Sunset: ${convertMilliToStringTime(viewModel.weather.other.sunset,
                    viewModel.weather.other.timeZone)}"
            }
        })

        tv_city.text = "${viewModel.weather.other.city},${viewModel.weather.other.country}"
        tv_weather.text = viewModel.weather.description

        tv_temp.text = convertKTOC(viewModel.weather.temp)

        tv_wind.text = "Wind: ${convertMPcToKMPH(viewModel.weather.wind)} km/h"
        tv_pressure.text = "Pressure: ${viewModel.weather.pressure} hpa"
        tv_humidity.text = "Humidity: ${viewModel.weather.humidity} %"
    }

}
