package com.example.weatherapp.view

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.weatherapp.Constants.APP_ID
import com.example.weatherapp.Constants.CURRENT_WEATHER_URL
import com.example.weatherapp.Constants.GOOGLE_API_KEY
import com.example.weatherapp.Constants.TIMEZONE_URL
import com.example.weatherapp.Helper
import com.example.weatherapp.Helper.convertMilliToStringDate
import com.example.weatherapp.Helper.getBackendAPI
import com.example.weatherapp.Helper.getTomorrowMillis
import com.example.weatherapp.interfaces.CompleteListener
import com.example.weatherapp.models.Weather
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {
    val logTag = "MainViewModel"
    val weather = Weather()
    lateinit var jsonObject: JSONObject
    lateinit var tempObject: JSONObject
    lateinit var windObject: JSONObject
    lateinit var sysObject: JSONObject
    var weatherArray = JSONArray()
    lateinit var weatherObject: JSONObject
    lateinit var cordObject: JSONObject

    val weatherList = ArrayList<Any>()
    val todayList = ArrayList<Any>()
    val tomorrowList = ArrayList<Any>()

    val todayFragment: TodayFragment
    val tomorrowFragment: TomorrowFragment
    val daysFragment: DaysFragment

    init {
        todayFragment = TodayFragment.newInstance()
        tomorrowFragment = TomorrowFragment.newInstance()
        daysFragment = DaysFragment.newInstance()
    }

    fun getWeather(city: String, completeListener: CompleteListener) {
        getBackendAPI(CURRENT_WEATHER_URL).getCurrentWeather(APP_ID, city)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.d(logTag, t.toString())
                    completeListener.onCancel()
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.d(logTag, response.toString())
                    if (response.isSuccessful) {
                        jsonObject = JSONObject(response.body()!!.string())
                        weather.time = jsonObject["dt"].toString().toLong()
                        weather.other.city = jsonObject["name"].toString()

                        getTempData(weather)
                        getWindData(weather)
                        getSysData(weather)
                        getCordData(weather)
                        getWeatherData(weather)

                        completeListener.onComplete()
                    }
                }
            })
    }

    fun getTimeZone(completeListener: CompleteListener) {
        getBackendAPI(TIMEZONE_URL).getTimeZone(GOOGLE_API_KEY, weather.time.toString(),
            "${weather.other.lat},${weather.other.lon}")
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    completeListener.onCancel()
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.d(logTag, response.toString())
                    if (response.isSuccessful) {
                        val jsonObject = JSONObject(response.body()!!.string())

//                        weather.other.timeZone = jsonObject["timeZoneId"].toString()

                        completeListener.onComplete()
                    }
                }
            })
    }

    fun getForecast(city: String, completeListener: CompleteListener) {
        weatherList.clear()
        todayList.clear()
        tomorrowList.clear()
        Helper.getBackendAPI(CURRENT_WEATHER_URL).getForecastWeather(APP_ID, city)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    completeListener.onCancel()
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    Log.d(logTag, response.toString())
                    if (response.isSuccessful) {
                        val jsonObj = JSONObject(response.body()!!.string())
                        var tempWeather: Weather
                        val listArray = JSONArray(jsonObj["list"].toString())

                        var isTodayDate = false
                        var isTomorrowDate = false
                        for (i in 1 until listArray.length()) {
                            tempWeather = Weather()
                            jsonObject = JSONObject(listArray[i].toString())

                            val date = jsonObject["dt_txt"].toString().trim().split(" ")[0]
                            if (i == 1) {
                                weatherList.add(date)
                            }

                            tempWeather.time = jsonObject["dt"].toString().toLong()
                            tempWeather.dateTime = jsonObject["dt_txt"].toString()

                            getTempData(tempWeather)
                            getWindData(tempWeather)
                            getWeatherData(tempWeather)

                            tempWeather.other.timeZone = weather.other.timeZone

                            weatherList.add(tempWeather)

                            if(date == convertMilliToStringDate(System.currentTimeMillis())){
                                if(!isTodayDate){
                                    todayList.add(date)
                                    isTodayDate = true
                                }
                                todayList.add(tempWeather)
                            }

                            if(date == convertMilliToStringDate(getTomorrowMillis())){
                                if(!isTomorrowDate){
                                    tomorrowList.add(date)
                                    isTomorrowDate = true
                                }
                                tomorrowList.add(tempWeather)
                            }

                            if (i < listArray.length() - 1) {
                                setDate(listArray, i)
                            }

                        }
                        completeListener.onComplete()
                    }
                }
            })
    }

    private fun setDate(list: JSONArray, index: Int) {
        val date = jsonObject["dt_txt"].toString().trim().split(" ")
        val date1 = JSONObject(list[index + 1].toString())["dt_txt"].toString().trim().split(" ")

        if (date[0] != date1[0]) {
            weatherList.add(date1[0])
        }
    }

    private fun getTempData(weather: Weather) {
        tempObject = JSONObject(jsonObject["main"].toString())

        weather.temp = tempObject["temp"].toString().toDouble()
        weather.pressure = tempObject["pressure"].toString().toDouble()
        weather.humidity = tempObject["humidity"].toString().toInt()
    }

    private fun getWindData(weather: Weather) {
        windObject = JSONObject(jsonObject["wind"].toString())
        weather.wind = windObject["speed"].toString().toDouble()
    }

    private fun getSysData(weather: Weather) {
        sysObject = JSONObject(jsonObject["sys"].toString())
        weather.other.sunrise = sysObject["sunrise"].toString().toLong()
        weather.other.sunset = sysObject["sunset"].toString().toLong()
        weather.other.country = sysObject["country"].toString()
    }

    private fun getCordData(weather: Weather) {
        cordObject = JSONObject(jsonObject["coord"].toString())
        weather.other.lat = cordObject["lat"].toString().toDouble()
        weather.other.lon = cordObject["lon"].toString().toDouble()
    }

    private fun getWeatherData(weather: Weather) {
        weatherArray = JSONArray(jsonObject["weather"].toString())
        weatherObject = JSONObject(weatherArray[0].toString())

        weather.icon = weatherObject["icon"].toString()
        weather.description = weatherObject["description"].toString()
    }

    fun notifyFragmentAdapter(){
        todayFragment.notifyAdapter()
        tomorrowFragment.notifyAdapter()
        daysFragment.notifyAdapter()
    }
}